import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchPostsAndUsers } from "../actions";
import UserHeader from "./UserHeader";

class PostList extends Component {
  componentDidMount() {
    this.props.fetchPostsAndUsers();
  }
  renderList() {
    return this.props.posts.map(post => {
      return (
        <div className="item" key={post.id}>
          <i className="icon large middle aligned user" />
          <div className="content">
            <div className="description">
              <h2>{post.title}</h2>
              <p>{post.body}</p>
            </div>
            <UserHeader userId={post.userId} />
          </div>
        </div>
      );
    });
  }
  render() {
    return <div className="ui relaxed divided list">{this.renderList()}</div>;
  }
}

const mapStateToProps = state => {
  return { posts: state.posts };
};

// 1. The first argument to connect is always "mapStateToProps"
// is there is no data, just pass null
// 2. The second argument is the action creator
export default connect(
  mapStateToProps,
  { fetchPostsAndUsers }
)(PostList);

/*
Every single time the reducer runs, "mapStateToProps" is going
to be called again. We return a new object with property props
and that object is going to show up as the props object inside
our component
*/
